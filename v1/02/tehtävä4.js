// Tehtävä 4
function potenssi(base, power) {
  if (power <= 0) return 1;
  
  return potenssiHelper(base, power, base);
}

function potenssiHelper(base, power, num) {
  if (power === 1) return num;
  else return potenssiHelper(base, power - 1, base * num);
}

console.log(potenssi(2,5));
console.log(potenssi(5,5));