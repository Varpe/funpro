var f = function () {
    return function (x,y) {
        if(x>y){
            return 1;
        }else if(x<y){
            return -1;
        }else return 0;
	 };
	}(); // Huomaa funktion kutsu!

var tulos = f(3,5);  // 3 < 5, pitäisi tulla 1
var tulos2 = f(5,3); // 5 > 3, pitäisi tulla -1
var tulos3 = f(5,5); // 5 = 5, pitäisi tulla 0
console.log(tulos);
console.log(tulos2);
console.log(tulos3);