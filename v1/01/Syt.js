//Tehtävä 2
var syt = function(p, q) {
    if (q==0) {
        return p;
    }

    return syt(p, p % q);
};

console.log(syt(50,2500));

// Tehtävä 3
var kjl = function(p,q){
  if(syt(p,q)==1) return true;
  return false;
}
console.log(kjl(1,1));
console.log(kjl(35,18));

// Tehtävä 4
function potenssi(a, b){
  if(b==0) return 1;
  return a * potenssi(a, --b);
}

console.log(potenssi(5,0));
console.log(potenssi(5,5));

// Tehtävä 5
function reverseArray (toBeReversed){
  var reversed = []; 

function reverser (toBeReversed){
    if (toBeReversed.length !== 0){     
      reversed.push( toBeReversed.pop() );
      reverser( toBeReversed );
    }
  }

  reverser(toBeReversed);
  return reversed;
}

console.log(reverseArray(["a","b","c"]));