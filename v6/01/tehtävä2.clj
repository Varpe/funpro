; Laske huhtikuun osalta muun nesteen kuin veden kulutus, siis tuloksena on yksi luku.

; Huhtikuun vedet yhteensä = 10,3
; Huhtikuun kokonaisnestemäärä = 20,6
; Muu kuin vesi = 20,6-10,3 = 10,3


(def food-journal
  [{:kk 3 :paiva 1 :neste 5.3 :vesi 2.0}
   {:kk 3 :paiva 2 :neste 5.1 :vesi 3.0}
   {:kk 3 :paiva 13 :neste 4.9 :vesi 2.0}
   {:kk 4 :paiva 5 :neste 5.0 :vesi 2.0}
   {:kk 4 :paiva 10 :neste 4.2 :vesi 2.5}
   {:kk 4 :paiva 15 :neste 4.0 :vesi 2.8}
   {:kk 4 :paiva 29 :neste 3.7 :vesi 2.0}
   {:kk 4 :paiva 30 :neste 3.7 :vesi 1.0}])


; Poistetaan maaliskuun tiedot   
(def food-journal2 (filter #(= (:kk %) 4) food-journal)) 

; Lasketaan huhtikuun vedet yhteen 

(def vesi (reduce + (map :vesi food-journal2)))

; Lasketaan huhtikuun kokonaisnestemäärä //

(def nesteet (reduce + (map :neste food-journal2)))

; Vähennetään kokonaisnesteestä veden osuus 
(- nesteet vesi)


