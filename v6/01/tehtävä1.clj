; Vuosien 2015 ja 2016 lämpötilat 

(def data [[-5.2 -3.4 0.5 3.5 7.2 10.5 12.3 14.4 9.8 7.9 3.2 1.4] [-4.6 -2.5 0.2 4.1 6.9 10.4 12.1 13.9 9.9 7.6 3.1 1.3]])

(defn average [coll] 
  (/ (reduce + coll) (count coll)))

(defn transpose [coll]
   (apply map vector coll))

(def sum #(reduce + %))
(def avg #(/ (sum %) (count %)))


; Lasketaan kuukausittaiset keskilämpötilat 

(def data2 (map average (transpose data)))
; Poistetaan negatiiviset arvot 

(remove neg? data2)

; Lasketaan lopuksi vielä keskilämpötila 

(avg data2)

