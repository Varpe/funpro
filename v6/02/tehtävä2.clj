;Oletetaan, että on määritelty vektori, joka sisältää vektoreita:

; [[1 2 3][4 5 6][7 8 9]].
; a) Esitä, kuinka tuotat apply-funktiota käyttäen seq:n, jossa on kunkin osavektorin minimiarvo.

(map #(apply min %) [[1 2 3][4 5 6][7 8 9]])
;(1 4 7)
; b) Esitä vielä, kuinka saat seq:n muutetuksi vektoriksi, joka sisältää alkioinaan nämä luvut, käyttäen pelkästään apply- ja vector-funktioita.

(vector (map #(apply min %) [[1 2 3][4 5 6][7 8 9]]))
;[(1 4 7)]