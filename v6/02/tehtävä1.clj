Haluat määritellä etukäteen erilaisia allekirjoitustapoja sähköposteihin liitettäviksi,
esim. muodollinen, epämuodollinen, suomeksi, englanniksi jne. Muodollisuuksien lisäksi sähköpostiin kirjoitetaan aina lähettäjän nimi 
jossain muodossa. Esitä, kuinka partial-funktiota hyväksi käyttäen voit luoda erilaisia funktioita, jolla voidaan helposti generoida allekirjoituksia siten,
että allekirjoitukseksi riittää antaa parametrina lähettäjän nimi.


(defn f
  "Sähköpostin allekirjoitus"
  [& args]
  (apply str args))

;englanniksi
(def eng (partial f "Sincerely, " "+358445145414, "))
;suomeksi, ilman suuntanumeroa
(def fin (partial f "Ystävällisin terveisin, "  "044 514 5414, "))
;sähköpostin kanssa ruotsiksi
(def swe (partial f "Mycket bra, " "+358445145414, " "juunas.andersson@metropolia.fi, "))

(eng "John Barkley") 
; "Sincerely, +358445145414, John Barkley"
(fin "Joonas Varpelaide")
; "Ystävällisin terveisin, 044 514 5414, Joonas Varpelaide"
(swe "Juunas Andersson")
; "Mycket bra, +358445145414, juunas.andersson@metropolia.fi, Juunas Åkesson"
