(ns test-lein.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(defn square [x] (* x x)
  
)

(defn karkausvuosi?
  [x]
  (cond (zero? (mod x 400)) true
        (zero? (mod x 100)) false
        (zero? (mod x 4)) true
        :default false))
