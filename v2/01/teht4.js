
var lampo2015 = [];
var lampo2016 = [];
var lammot = [];
var r2=0;
var r1=0;
var h=0;

// Funktio satunnaisten lämpötilojen luontiin halutulle välille
function arvoLampotila(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
}


// Luodaan satunnaiset lämpötilat väliltä -15 - 25 astetta taulukoihin
for(var i=0; i<12; i++){
    r1 = arvoLampotila(-15, 25);
    r2 = arvoLampotila(-15, 25);
    lampo2015.push(r1);
    lampo2016.push(r2);
}
// Tulostetaan molempien vuosien lämpötilat
console.log("Vuoden 2015 lämpötilat: " + lampo2015);
console.log("Vuoden 2016 lämpötilat: " + lampo2016);


// Lasketaan kahden vuoden keskiarvot
for(i=0; i<lampo2015.length;i++){
    lammot.push(((lampo2015[i]+lampo2016[i]))/2);
}
console.log("Vuoden 2015 ja 2016 keskiarvot: "+ lammot);

// Poistetaan taulukosta pakkasarvot
lammot = lammot.filter(function(lammot){
    return lammot >= 0;
});

console.log("Taulukosta filteriöidyt positiiviset lukemat: "+ lammot);

// Lasketaan vielä kahden vuoden keskilämpötilojen keskiarvo
var maara = lammot.length;
lammot = lammot.reduce((a, b) => a += b);
lammot /= maara;

console.log("Kahden vuoden keskilämpötila: "+ lammot);
