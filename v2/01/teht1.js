
// a)
var toCelsius = fahrenheit => (((fahrenheit-32)*(5/9)));

console.log(toCelsius(10));


// b)    
var area = radius => (Math.PI * radius * radius);

console.log(area(10));
