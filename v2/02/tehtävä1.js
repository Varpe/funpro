function laskePisteet(pituus, kPiste, lisapisteet){
    if(pituus>=kPiste){
        return 60 + ((pituus-kPiste)*lisapisteet);
    }else if(pituus<kPiste){
        return ((pituus-kPiste)*(lisapisteet));
    }
}

// Currying
function pisteyttaja(kPiste,lisapisteet){
    return function(pituus){
        return laskePisteet(pituus, kPiste, lisapisteet);
    };
}
// Partial application
    const lahtiNormaali = pisteyttaja(82 ,1.8);  // Lahden normaalimäki, K-piste 82m
    const lahtiLento = pisteyttaja(120 ,1.8);   // Lahden lentomäki, K-piste 120m
    
    console.log("84 metrin hyppy Lahden normaalimäessä, pisteet: " + lahtiNormaali(84));
    console.log("123.5 metrin hyppy Lahden lentomäessä, pisteet: " + lahtiLento(123.5));

