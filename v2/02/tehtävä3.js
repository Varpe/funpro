const Immutable = require('immutable');
const set1 = Immutable.Set(['punainen', 'vihreä', 'keltainen']);

set2 = set1 & 'ruskea';

console.log(set1 === set2); //False

set2 = set2 & 'ruskea';

set3 = set2 & 'ruskea';

console.log(set2 === set3); //True