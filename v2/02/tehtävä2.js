'use strict';
const Auto  = (function(){
    
    const suojatut = new WeakMap();  // yhteinen ilmentymille
    var tankki = 50;
    var matka = 0;
    class Auto{
        constructor(){
            
            suojatut.set(this, {matkamittari: matka});
        }  // suojattu instanssimuuttuja matkamittarille
        
     
        getMatka() {return suojatut.get(this).matkamittari;}
        getTankki() {return tankki;}
        tankkaa(x){
            console.log("Tankissa oli jäljellä: " + tankki +" litraa bensaa.");
            tankki = tankki + x;
            console.log("Tankkauksen jälkeen tankissa: " + tankki + " litraa");
        }
        aja(y){
            console.log("Vroom vroom, autolla päristeltiin "+y+" kilometriä.");
            matka=matka+y;
            console.log("Matkaan kului bensaa: " + (9.2/100*y) + " litraa.");
            tankki = tankki - (9.2/100*y);
            console.log("Tankissa jäljellä: " + tankki + " litraa");
            console.log("Matkamittari: "+matka+" km");
            
        }

        
    }
    
    return Auto;
})();

    //Luodaan uusi auto
    const ford = new Auto();
    console.log("Uusi ford saapui tehtaalta!");
    console.log("Matkamittarilukema: "+ ford.getMatka()) + " km";
    console.log("Bensamittarilukema: "+ ford.getTankki()) + " litraa";
    
    // Ajetaan 15kilometriä
    console.log(ford.aja(15));
    
    // Käydään tankilla ottamassa 30 litraa bensaa
    console.log(ford.tankkaa(30));
    
    // Ajetaan vielä 7 kilometriä
    console.log(ford.aja(7));
    
    