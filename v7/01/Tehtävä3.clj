;Määritetään kalevala teksti file muuttujaan
(def file "kalevala.txt")
;Käytetään slurp jotta saadaan stringiksi
(def laskettavat (slurp file))

(frequencies (clojure.string/split laskettavat #"\s"))
