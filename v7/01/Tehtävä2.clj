;Suurin yhteinen tekijä (p, q) 

(defn syt [p q]
    (if (zero? q)
      p
      (recur q (mod p q)))
)
(syt 102 68)
;34